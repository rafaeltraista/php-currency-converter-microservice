<?php

$pairs = array(
	"eur/usd" => 1.132625,
	"usd/eur" => 0.882904757,
	"eur/ron" => 4.75407462,
	"ron/eur" => 0.210345878,
	"usd/ron" => 4.1973951,
	"ron/usd" => 0.238243
);

$return_message = array(
	"code"=>0,
	"error_code"=>"Return success.",
	"value"=>"-1"
);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if(isset($_GET["incurrency"]) && isset($_GET["outcurrency"]) && isset($_GET["value"])){
        $pair = $_GET["incurrency"] . '/' . $_GET["outcurrency"];
        if(array_key_exists($pair, $pairs)){
            $ratio = $pairs[$pair];
        	$value = (float)($_GET["value"]) * $ratio;
            $return_message["value"] = $value;  
        }else{
            $return_message["code"] = 3;
    	    $return_message["error_code"] = "Unknown pair";
        }
    }else{
    	$return_message["code"] = 1;
    	$return_message["error_code"] = "Wrong request format";
    }
}else{
    $return_message["code"] = 2;
	$return_message["error_code"] = "Only GET requests are accepted";
}



echo json_encode($return_message);
?>